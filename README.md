# Turbochat

This is a study project to play with features introduced in Turbo 8 (prefetch, broadcasts, …)
and Rails 7 (`rails new --css tailwind`, Propshaft, ImportMaps, …). 

## License

MIT license, see [LICENSE.md](LICENSE.md).
