# frozen_string_literal: true

class MessagesController < ApplicationController
  before_action :set_room
  before_action :set_message, only: :destroy

  def create
    message = @room.messages.build(message_params)
    if message.save
      render turbo_stream: [
        turbo_stream.replace(:message_count, partial: "rooms/message_count", locals: { room: @room }),
        turbo_stream.append(:message_list, partial: "messages/message", locals: { message: message }),
        turbo_stream.replace(:message_form, partial: "messages/form", locals: { message: @room.messages.build(author: message.author) }),
      ]
    else
      render turbo_stream: [
        turbo_stream.replace(:message_form, partial: "messages/form", locals: { message: message }),
      ]
    end
  end

  def destroy
    @message.destroy!
    render turbo_stream: [
      turbo_stream.replace(:message_count, partial: "rooms/message_count", locals: { room: @room }),
      turbo_stream.remove(@message),
    ]
  end

  private

  def message_params
    params.require(:message).permit(
      :author,
      :content
    )
  end

  def set_room
    @room = Room.find(params[:room_id])
  end

  def set_message
    @message = @room.messages.find(params[:id])
  end
end
