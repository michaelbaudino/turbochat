class Message < ApplicationRecord
  broadcasts_refreshes_to :room

  belongs_to :room
  validates :author, :content, presence: true
end
