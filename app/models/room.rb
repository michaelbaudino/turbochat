class Room < ApplicationRecord
  broadcasts_refreshes

  has_many :messages, -> { order(created_at: :asc) }, dependent: :destroy
end
